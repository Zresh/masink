package com.example.zresh.masink;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    public static long serverTime;
    public static long timeOffset;
    public WebView mWebView;
    public TextView textView;
    public TextView textView2;

    private long getServerTime() {
        mWebView.loadUrl("javascript:Android.getServerTime(ts.now());");
        return serverTime;
    }

    private long getTimeOffset() {
        mWebView.loadUrl("javascript:Android.getTimeOffset(lastOffset);");
        return timeOffset;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWebView = new WebView(this);
        mWebView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mWebView.getSettings().setAllowFileAccessFromFileURLs(true);
            mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        }
        mWebView.loadUrl("http://46.101.111.38:3000");
        mWebView.addJavascriptInterface(new CustomJavaScriptInterface(mWebView.getContext()), "Android");

        textView = (TextView) findViewById(R.id.text1);
        textView2 = (TextView) findViewById(R.id.text2);
            long a = getServerTime();
            long b = getTimeOffset();
    }


}
