package com.example.zresh.masink;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;

import static android.content.ContentValues.TAG;

public class CustomJavaScriptInterface {
    Context mContext;

    /**
     * Instantiate the interface and set the context
     */
    CustomJavaScriptInterface(Context c) {
        mContext = c;
    }

    /**
     * retrieve the server time
     */
    @JavascriptInterface
    public void getServerTime(final long time) {
        Log.d(TAG, "Time is " + time);
        MainActivity.serverTime = time;
    }

    @JavascriptInterface
    public void getTimeOffset(final long offset) {
        Log.d(TAG, "Time offset is " + offset);
        MainActivity.timeOffset = offset;
    }
}